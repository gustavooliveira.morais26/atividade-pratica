<img src="https://apexensino.com.br/wp-content/uploads/2017/03/linguagens-de-programa%C3%A7%C3%A3o.jpg">
<h1><b>RESPOSTAS</b></h1>
<ul>
<b>Questão 03</b>
<li> Esse código faz a soma de dois números passados pelo usuário. Para executar o código, basta termos o Node JS instalado e utilizar o comando: node calculadora.js (x) (y) onde x e y são números. O código então fará a soma dos dois valores.</li>
<hr>
<b>Questão 04</b>
<li>Ao invés de utilizar git add . , utilizaei git add calculadora.js para adicionar apenas esse arquivo. Em relação ao código,  seu commit deve ser do tipo feat, haja vista que, foram incluídas novas funções no projeto. Já o commit do READ.me deve ser do tipo docs, já que ele é apenas um documento de referência</li>
<hr>
<b>Questão 05</b>
<li>Esse comit deve ser do tipo refactor, pois não há adição de funcionalidades, apenas uma reescrita do código de outra forma</li>
<hr>
<b>Questão 06</b>
<li>O erro do código está no índice dos args. Para utilizar o código, basta escolher soma ou subtração e em seguida dois números para que a operação escolhida seja realizada</li>
<hr>
<b>Questão 07</b>
<li>Basta usar o comando git checkout -b (nome da nova branch) </li>
<hr>
<b>Questão 08</b>
<li>Basta acessar “Create merge request” e digitar no campo “Source (branch ou tag)” o nome da branch na qual iremos trabalhar e por fim clicar em “Create merge request”.</li>
<hr>
<b>Questão 09</b>
<li>Utilizando o comando $ git revert HEAD, é realizado um novo commit com os dados do antecessor, voltando o código ao que era antes das alterações de joãozinho</li>
</ul>
